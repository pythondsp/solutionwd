Question: Meltdown problem
==========================

.. admonition:: Codes and Datasets
   :class: danger
   
   The datasets and the codes of the tutorial can be downloaded from the `repository <https://bitbucket.org/pythondsp/solutionwd/downloads/>`_
   

Question : Let's assume that n processes are running on the farm. They run forever, never die, and no new processes get spawned. Each process eats memory at a constant, individual rate - process p_i (with 0 <= i < n) consumes 1 byte after every d(p_i) seconds. The total amount of available disk space is denoted by X.  

For each given input configuration (read from stdin), calculate the ETA in seconds.

HINT � it is not the average

A configuration is encoded as a single line like this: 

X d(p_1) d(p_2) ... d(p_n)

Each output (the number of seconds) should be written as a single line to stdout.

Example:


$ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python your_solution.py

> 9

> 12

> 14

Solution to meltdown problem
============================

Codes can be downloaded from Bitbucket.
https://bitbucket.org/pythondsp/solutionwd/src/1853a294a3f0dc9ff9fbb29b54340b323d7c6971/ETA/?at=master


The solution is divided into two parts. 

* In first part, inputs are read from the 'stdin'. Also, various problems with the code are shown for large data. 

* In part 2, the problems are removed and a separate function is written for calculating the 'next meltdown'. Further, Numba is used to increase the speed of the simulation. 


Part 1: Bad solution
--------------------

Here, inputs are read from 'stdin'. The solution is quite simple, but has several problems for large-data scenario. Problems are shown in docstring of function 'bad_ETA'.  Following is the code for calculating the 'next meltdown', 

.. code-block:: python

    # bad_solution.py 

    import sys
    import numpy as np
    import pandas as pandas

    def bad_ETA(x):
        """ increment i by 1 step and
            check for completion of each process
        
            Problems:
                1. i is incremented by 1, which result in longer simulation
                2. each process is stored as python-array
                3. simple python-loop is used to check the completed process
        """

        # extract 'memory' and' 'process-duration (data)'
        memory = x[0]
        data = sorted(x[1:])

        i = 0
        j = 0
        while True:
            i = i + 1
            for d in data:
                if i%d==0:  # check for process completion
                    j = j + 1
                    if j >= memory:  # if full, return i
                        return i
        
    def main():
        """ calculate next meltdown from stdin inputs 

        execution: 
        $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
        """

        # read all lines from stdin
        read_sys = sys.stdin.readlines()
        data=[]

        # save data in list
        for d in read_sys:
            # read data and convert into int
            dd = [int(d) for d in np.array(d.split())]
            data.append(dd)

        # calculate next meltdown for all data
        for d in data:
            print(">", bad_ETA(d))

    if __name__=='__main__':
        main()

* Following are the outputs of above code, 

.. code-block:: shell

    $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python bad_solution.py
    > 9
    > 12
    > 14



Part 2: Optimized Solution
--------------------------

Following changes are made to previous code, to increase the speed of the code, 

fast_solution.py
^^^^^^^^^^^^^^^^

Here, bisection search method is used for iteration. Hence, iteration variable jumps by half the range (backward or forward), instead of moving forward with one step ( i.e. i in previous section). In this way, the code is optimized for large data. Also, some other modification are done, which are shown as comment 

.. code-block:: python

    # fast_solution.py

    import numpy as np

    def ETA(data):
        """ calculate next meltdown for data

            outputs:
                $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
                > 9
                > 12
                > 14

                $ python file_input.py 
                9
                12
                14
                590619548
                181210921829
                204221389795918291262975
                323
        """

        # extract 'memory' and' 'process-duration (pd)'
        memory = data[0]   # memory
        pd = data[1:]      # process-duration
        pd = np.array(pd)  # convert to numpy arrays


        # initialization    
        current = memory           # current ETA
        prev_c  = memory//2        # previous ETA 
        next_c = current           # next ETA
        sum_memory = sum(current//pd)  # sum of memory consumed

        ## test: uncomment below to quick checks for answers
        ## and run the command $ python file_input.py
        ## put 'current' and 'current+1' below, to see the correctness of the code
        # sum_memory = sum(324//pd)  
        # return sum_memory


        # if sum_memory is less than memory,
        # then increase the initial count as bisection-search
        # will go above the memory. e.g. case 1 i.e. 4 3 7
        i=1
        while sum_memory < memory:
            current, prev_c = i * next_c, current
            sum_memory = sum(current//pd)
            i = i + 1


        while True:
            sum_memory = sum(current//pd)
            # print(memory, current, prev_c, next_c, sum_memory)

            # store 'current' as 'next_c' and recalculate the current.
            # next_c is used because after calculation 'current' value will be
            # less than itself.
            if sum_memory > memory:
                next_c, current = current, (prev_c + current)//2

                # set prev_c to 0, if all equal to continue the search
                if current == prev_c and prev_c==next_c:
                    prev_c = 0

            elif sum_memory < memory:
                prev_c, current = current, (next_c+current)//2

                # return current if current and prev_c are equal
                # example line 4 of the file 'proc_disk_usage.data'
                # memory, current, prev_c, sum_memory
                # 1000000000000 181210921830 181210921829 1000000000016
                # 1000000000000 181210921829 181210921829 999999999979
                if current == prev_c:
                    return current

            # decrease value of current and check again till condition
            # sum_memory == memory is valid. Ex. 15 2 4 5 6
            elif sum_memory == memory:
                # print(current)
                # current = current - 1
                # sum_memory = sum(current/pd)
                while sum_memory == memory:
                    current = current - 1
                    sum_memory = sum(current//pd)
                return current + 1


sys_input.py
^^^^^^^^^^^^

Following are the content of the file, which reads the inputs from sys\_in and convert these values to int-array, 

.. code-block:: python

    # sys_input.py

    import sys
    import numpy as np

    from fast_solution import ETA

    def main():
        """ calculate next meltdown from stdin inputs 

        execution: 
        $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
        """

        # read all lines from stdin
        read_sys = sys.stdin.readlines()
        data=[]

        # save data in list
        for d in read_sys:
            dd = [int(d) for d in np.array(d.split())]
            data.append(dd)

        # calculate next meltdown for all data
        for d in data:
            print(">", ETA(d))

    if __name__=='__main__':
        main()


file_input.py
^^^^^^^^^^^^^

Following are the content of the file, which read the data from the file and covert into int-array. 

.. code-block:: python

    
    # file_input.py 

    import numpy as np
    from fast_solution import ETA

    def main():
        """ Read data from file and 
            Convert data into integer format 
        """
        with open('proc_disk_usage.data', 'r') as f:
            for line in f:
                # extract data from file
                line = line.strip()
                parts = line.split(' ')

                # convert string data to float
                # float is used (instead of int) for Numba
                for i,part in enumerate(parts):
                    parts[i] = int(part)

                parts = np.array(parts)

                ## analyse the data
                # print(sorted(parts)[-10:])
                # print(parts[0], min(parts), max(parts), len(parts))


                # print next meltdown
                print(ETA(parts))

    if __name__=='__main__':
        main()

Outputs with test-results
-------------------------



Outputs
^^^^^^^


Following are the outputs of above codes, 

.. code-block:: shell

    $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
    > 9
    > 12
    > 14

    $ python file_input.py 
    9
    12
    14
    590619548
    181210921829
    204221389795918291262975
    323

Test-results
^^^^^^^^^^^^

Memory      : Available disk space

ETA         : Calculated meltdown

M_ETA       : Memory consumed at ETA

Next_ETA    : Next ETA where memory consumed is greater than 'Memory'

M_Next_ETA  : Memory consumed at Next_ETA


.. table::  Test is passed if 'M_ETA <= Memory < M_Next_ETA'

    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | Row | Memory | ETA                      | M_ETA                    | Next_ETA                 | M_Next_ETA | Test   |
    +=====+========+==========================+==========================+==========================+============+========+
    | 1   | 4      | 9                        | 4                        | 12                       | 5          | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 2   | 15     | 12                       | 15                       | 14                       | 16         | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 3   | 16     | 14                       | 16                       | 15                       | 17         | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 4   | 10^6   | 590619548                | 1000000                  | 590621093                | 10^6 + 3   | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 5   | 10^12  | 181210921829             | 999999999979             | 181210921830             | 10^12 + 16 | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 6   | 10^16  | 204221389795918291262975 | 999999999999999999999972 | 204221389795918291262976 | 10^16 + 9  | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+
    | 7   | 1000   | 323                      | 1000                     | 324                      | 1010       | Passed |
    +-----+--------+--------------------------+--------------------------+--------------------------+------------+--------+


