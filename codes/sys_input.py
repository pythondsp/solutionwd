# sys_input.py

import sys
import numpy as np

from fast_solution import ETA

def main():
    """ calculate next meltdown from stdin inputs 

    execution: 
    $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
    """

    # read all lines from stdin
    read_sys = sys.stdin.readlines()
    data=[]

    # save data in list
    for d in read_sys:
        dd = [int(d) for d in np.array(d.split())]
        data.append(dd)

    # calculate next meltdown for all data
    for d in data:
        print(">", ETA(d))

if __name__=='__main__':
    main()

