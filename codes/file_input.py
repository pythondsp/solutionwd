# file_input.py 

import numpy as np
from fast_solution import ETA

def main():
    """ Read data from file and 
        Convert data into integer format 
    """
    with open('proc_disk_usage.data', 'r') as f:
        for line in f:
            # extract data from file
            line = line.strip()
            parts = line.split(' ')

            # convert string data to float
            # float is used (instead of int) for Numba
            for i,part in enumerate(parts):
                parts[i] = int(part)

            parts = np.array(parts)

            ## analyse the data
            # print(sorted(parts)[-10:])
            # print(parts[0], min(parts), max(parts), len(parts))


            # print next meltdown
            print(ETA(parts))

if __name__=='__main__':
    main()
